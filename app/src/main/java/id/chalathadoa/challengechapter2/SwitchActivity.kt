package id.chalathadoa.challengechapter2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button

class SwitchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_switch)
        openActivityOne()
        openActivityTwo()
    }
    private fun openActivityOne(){
        val buttonOne = findViewById<Button>(R.id.gojekLayout)
        buttonOne.setOnClickListener(){
            val intent = Intent(this, GojekActivity::class.java)
            startActivity(intent)
        }
    }
    private fun openActivityTwo(){
        val buttonTwo = findViewById<Button>(R.id.tokpedLayout)
        buttonTwo.setOnClickListener(){
            val intent = Intent(this, TokpedActivity::class.java)
            startActivity(intent)
        }
    }
}